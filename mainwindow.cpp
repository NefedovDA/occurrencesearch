#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QCommonStyle>
#include <QDesktopWidget>

#include <QClipboard>
#include <QDesktopServices>
#include <QDirIterator>
#include <QFileDialog>
#include <QMessageBox>
#include <QtConcurrent/QtConcurrentFilter>
#include <QtConcurrent/QtConcurrentMap>
#include <QtConcurrent/QtConcurrentRun>

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , currentState()
    , data(new WorkingData(""))
    , needStopProcess(0)
{
    qRegisterMetaType<QVector<TrigramSet>>("QVector<TrigramSet>");
    qRegisterMetaType<QVector<int>>("QVector<int>");

    ui->setupUi(this);
    setGeometry(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, size(), qApp->desktop()->availableGeometry()));

    ui->treeWidget->header()->setSectionResizeMode(0, QHeaderView::Stretch);
    ui->treeWidget->header()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    ui->treeWidget->setContextMenuPolicy(Qt::CustomContextMenu);

    QCommonStyle style;
    ui->actionInstruction->setIcon(style.standardIcon(QCommonStyle::SP_DialogHelpButton));
    ui->actionChouseDirectory->setIcon((style.standardIcon(QCommonStyle::SP_DialogOpenButton)));
    ui->actionStartSearch->setIcon(style.standardIcon(QCommonStyle::SP_MediaPlay));
    ui->actionRefresh->setIcon(style.standardIcon(QCommonStyle::SP_BrowserReload));
    ui->actionClear->setIcon(style.standardIcon(QCommonStyle::SP_DialogResetButton));
    ui->actionStopProcess->setIcon(style.standardIcon(QCommonStyle::SP_MediaStop));
    ui->actionExit->setIcon(style.standardIcon(QCommonStyle::SP_DialogCloseButton));

    connect(ui->actionAboutQt, &QAction::triggered, this, &MainWindow::showAboutQTDialog);
    connect(ui->actionInstruction, &QAction::triggered, this, &MainWindow::showInstructionDialog);
    connect(ui->actionChouseDirectory, &QAction::triggered, this, &MainWindow::selectDirectory);
    connect(ui->actionStartSearch, &QAction::triggered, this, &MainWindow::startSearch);
    connect(ui->actionRefresh, &QAction::triggered, this, &MainWindow::refresh);
    connect(ui->actionClear, &QAction::triggered, this, &MainWindow::clearAll);
    connect(ui->actionExit, &QAction::triggered, this, &MainWindow::close);

    connect(ui->actionStopProcess, &QAction::triggered, this, &MainWindow::stopProcess);

    connect(&scaner, &QFutureWatcher<void>::started, ui->progressBar, &QProgressBar::reset);
    connect(&scaner, &QFutureWatcher<void>::progressRangeChanged, ui->progressBar, &QProgressBar::setRange);
    connect(&scaner, &QFutureWatcher<void>::progressValueChanged, ui->progressBar, &QProgressBar::setValue);
    connect(&scaner, &QFutureWatcher<void>::finished, this, &MainWindow::endScaning);
    connect(ui->actionStopProcess, &QAction::triggered, &scaner, &QFutureWatcher<void>::cancel);

    connect(&indexer, &QFutureWatcher<void>::started, ui->progressBar, &QProgressBar::reset);
    connect(&indexer, &QFutureWatcher<void>::progressRangeChanged, ui->progressBar, &QProgressBar::setRange);
    connect(&indexer, &QFutureWatcher<void>::progressValueChanged, ui->progressBar, &QProgressBar::setValue);
    connect(&indexer, &QFutureWatcher<void>::finished, this, &MainWindow::endIndexing);
    connect(ui->actionStopProcess, &QAction::triggered, &indexer, &QFutureWatcher<void>::cancel);

    connect(&filter, &QFutureWatcher<void>::started, ui->progressBar, &QProgressBar::reset);
    connect(&filter, &QFutureWatcher<void>::progressRangeChanged, ui->progressBar, &QProgressBar::setRange);
    connect(&filter, &QFutureWatcher<void>::progressValueChanged, ui->progressBar, &QProgressBar::setValue);
    connect(&filter, &QFutureWatcher<void>::finished, this, &MainWindow::endWork);
    connect(ui->actionStopProcess, &QAction::triggered, &filter, &QFutureWatcher<void>::cancel);

    connect(&searcher, &QFutureWatcher<void>::started, ui->progressBar, &QProgressBar::reset);
    connect(&searcher, &QFutureWatcher<void>::progressRangeChanged, ui->progressBar, &QProgressBar::setRange);
    connect(&searcher, &QFutureWatcher<void>::progressValueChanged, ui->progressBar, &QProgressBar::setValue);
    connect(&searcher, &QFutureWatcher<void>::finished, this, &MainWindow::endWork);
    connect(ui->actionStopProcess, &QAction::triggered, &searcher, &QFutureWatcher<void>::cancel);

    connect(ui->treeWidget, &QTreeWidget::itemDoubleClicked, this, &MainWindow::openItemFile);
    connect(ui->treeWidget, &QTreeWidget::customContextMenuRequested, this, &MainWindow::showContextMenu);

    updateState(NoWork);
}

void MainWindow::openItemFile(QTreeWidgetItem* item)
{
    QString path = item->text(0);
    if (QFileInfo(path).isExecutable()) {
        QMessageBox::warning(this, "Opening file", "File could not be opened.", QMessageBox::Ok);
        return;
    }
    bool success = QDesktopServices::openUrl(QUrl::fromLocalFile(path));
    if (!success) {
        QMessageBox::warning(this, "Opening file", "File could not be opened.", QMessageBox::Ok);
    }
}

void MainWindow::showContextMenu(QPoint const& position)
{
    QTreeWidgetItem* item = getSelectedItem();
    if (item == nullptr) {
        return;
    }

    QMenu* menu = new QMenu(this);

    QAction* openAction = new QAction("Open file", this);
    QAction* copyAction = new QAction("Copy path", this);

    connect(openAction, &QAction::triggered, this, &MainWindow::findContextForOpen);
    connect(copyAction, &QAction::triggered, this, &MainWindow::copyPath);

    menu->addAction(openAction);
    menu->addAction(copyAction);

    menu->popup(ui->treeWidget->viewport()->mapToGlobal(position));
}

void MainWindow::copyPath()
{
    QTreeWidgetItem* item = getSelectedItem();
    QString path = item->text(0);

    QApplication::clipboard()->setText(path);
}

QTreeWidgetItem* MainWindow::getSelectedItem()
{
    auto const& list = ui->treeWidget->selectedItems();
    return *list.begin();
}

void MainWindow::findContextForOpen()
{
    openItemFile(getSelectedItem());
}

MainWindow::~MainWindow()
{
}

void MainWindow::addLine(QString path, qint64 count)
{
    QTreeWidgetItem* item = new QTreeWidgetItem(ui->treeWidget);
    item->setText(0, path);
    item->setText(1, QString::number(count));
    ui->treeWidget->addTopLevelItem(item);
}

void MainWindow::showAboutQTDialog()
{
    QMessageBox::aboutQt(this);
}

void MainWindow::showInstructionDialog()
{
    QMessageBox instrictionDialog(QMessageBox::NoIcon,
        "Instruction",
        "Hi! For search occurrence of some line in files of some directory and it's subdirectorys do:\n"
        "1) Chouse directoryh\n"
        "2) Inpute line for search\n"
        "3) Look at result!",
        QMessageBox::Ok, this);
    instrictionDialog.exec();
}

void MainWindow::selectDirectory()
{
    QString dir = QFileDialog::getExistingDirectory(this, "Select directory", QString(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if (dir == "") {
        return;
    }

    if (dir == data->dir) {
        QMessageBox::information(this, "Select directory", "Current directory already selected", QMessageBox::Ok);
        return;
    } else {
        data = std::shared_ptr<WorkingData>(new WorkingData(dir));
    }

    updateState(WaitResult);
    startScaning();
}

void MainWindow::startSearch()
{
    QString line = ui->lineEdit->text();

    // do work
    TrigramSet source;
    QByteArray buffer = line.toLatin1();
    if (buffer.length() < 3) {
        QMessageBox::information(this, "Start search", "Too litle line", QMessageBox::Ok);
        return;
    }

    updateState(WaitResult);
    setWindowTitle("Searching...");

    TrigramSet::addToSet(source, buffer.data(), buffer.length());
    searcher.setFuture(QtConcurrent::map(data->trigramsets, [this, source, line](TrigramSet const& trigtamSet) { doSearch(trigtamSet, source, line); }));
}

void MainWindow::refresh()
{
}

void MainWindow::clearAll()
{
    ui->lineEdit->clear();
    ui->treeWidget->clear();
}

void MainWindow::endWork()
{
    updateState(WaitInputLine);
}

void MainWindow::stopProcess()
{
    needStopProcess = 1;
    updateState(NoWork);
}

void MainWindow::startScaning()
{
    // start work
    setWindowTitle("Scaning...");
    scaner.setFuture(QtConcurrent::run([this]() { doScan(); }));
    // then indexing
    // then filtering
    // then endWork();
}

void MainWindow::doScan()
{
    QString dir = data->dir;

    QDirIterator it(dir, QDir::NoDotAndDotDot | QDir::Dirs | QDir::Files | QDir::NoSymLinks, QDirIterator::Subdirectories);
    while (it.hasNext()) {
        if (needStopProcess == 1) {
            return;
        }
        QString path = it.next();
        if (QFileInfo(path).isFile()) {
            TrigramSet trigramSet(path);
            data->trigramsets.append(trigramSet);
        } else {
            data->watcher.addPath(path);
        }
    }
}

void MainWindow::endScaning()
{
    if (needStopProcess == 1) {
        return;
    }
    setWindowTitle("Indexing...");
    indexer.setFuture(QtConcurrent::map(data->trigramsets, &TrigramSet::processFile));
}

void MainWindow::endIndexing()
{
    setWindowTitle("Filtering...");
    filter.setFuture(QtConcurrent::filter(data->trigramsets, [this](TrigramSet const& trigramSet) { return trigramSet.goodFile; }));
}

void MainWindow::doSearch(TrigramSet const& trigramSet, TrigramSet source, QString inputLine)
{
    if (trigramSet.isSubSet(source)) {
        qint64 count = trigramSet.findLine(inputLine);
        if (count != 0) {
            QMutexLocker locker(new QMutex());
            addLine(trigramSet.filePath, count);
        }
    }
}

void MainWindow::updateState(MainWindow::State state)
{
    setWindowTitle("Occurence Search");
    ui->progressBar->reset();
    switch (state) {
    case NoWork:
        data->dir = "";
        clearAll();
        functionality(false, false);
        break;
    case WaitInputLine:
        functionality(true, false);
        break;
    case WaitResult:
        needStopProcess = 0;
        ui->treeWidget->clear();
        functionality(false, true);
        break;
    }
}

void MainWindow::functionality(bool activityMode, bool processMode)
{
    activityMode = !activityMode;
    processMode = !processMode;

    ui->lineEdit->setDisabled(activityMode);
    ui->actionStartSearch->setDisabled(activityMode);
    ui->actionRefresh->setDisabled(activityMode);
    ui->actionClear->setDisabled(activityMode);

    ui->actionChouseDirectory->setDisabled(!processMode);
    ui->actionStopProcess->setDisabled(processMode);
    ui->progressBar->setDisabled(processMode);
}

WorkingData::WorkingData(QString dir)
    : QObject()
    , dir(dir)
    , supdirs()
    , files()
    , watcher()
    , trigramsets()
{
    connect(&watcher, &QFileSystemWatcher::fileChanged, this, &WorkingData::setFileUpdateFlag);
    connect(&watcher, &QFileSystemWatcher::directoryChanged, this, &WorkingData::setSubdirUpdateFlag);
}
