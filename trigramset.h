#ifndef TRIGRAM_H
#define TRIGRAM_H

#include <QSet>
#include <QString>
#include <QVector>

class TrigramSet {
public:
    TrigramSet(QString filePath = "");

    ~TrigramSet();

    QSet<quint32> set;
    QString filePath;
    bool goodFile;

    void markBad();
    bool isSubSet(TrigramSet const& source) const;
    qint64 findLine(const QString& line) const;

    static QSet<QString> warnings;

    static const qint64 MAX_LINE_LENGTH = 2000;
    static const qint64 MAX_TRIGRAMS = 20000;

    static void processFile(TrigramSet& trigramSet);
    static void addToSet(TrigramSet& trigramSet, char* buffer, qint64 length);

    static qint64 boyerMoore(QString const& text, QString const& source);
};

#endif // TRIGRAM_H
