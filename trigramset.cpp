#include "trigramset.h"

#include <QFile>
#include <QMutexLocker>
#include <algorithm>
#include <functional>

TrigramSet::TrigramSet(QString filePath)
    : set()
    , filePath(filePath)
    , goodFile(true)
{
}

TrigramSet::~TrigramSet()
{
}

void TrigramSet::markBad()
{
    goodFile = false;
    set.clear();
}

bool TrigramSet::isSubSet(const TrigramSet& source) const
{
    for (quint32 trigram : source.set) {
        if (set.find(trigram) == set.end()) {
            return false;
        }
    }
    return true;
}

qint64 TrigramSet::findLine(QString const& line) const
{
    char buffer[MAX_LINE_LENGTH];

    QFile file(filePath);
    if (!file.open(QIODevice::ReadOnly)) {
        QMutexLocker locker(new QMutex());
        warnings.insert(filePath);
        return 0;
    }

    qint64 count = 0;
    while (!file.atEnd()) {
        qint64 length = file.readLine(buffer, MAX_LINE_LENGTH);
        if (length == -1) {
            QMutexLocker locker(new QMutex());
            warnings.insert(filePath);
            return 0;
        }
        count += boyerMoore(QString(buffer), line);
    }
    return count;
}

QSet<QString> TrigramSet::warnings;

void TrigramSet::processFile(TrigramSet& trigramSet)
{
    char buffer[MAX_LINE_LENGTH];

    QFile file(trigramSet.filePath);
    if (!file.open(QIODevice::ReadOnly)) {
        QMutexLocker locker(new QMutex());
        warnings.insert(trigramSet.filePath);
        trigramSet.markBad();
        return;
    }

    while (!file.atEnd()) {
        qint64 length = file.readLine(buffer, MAX_LINE_LENGTH);
        if (length == -1) {
            QMutexLocker locker(new QMutex());
            warnings.insert(trigramSet.filePath);
            trigramSet.markBad();
            return;
        }
        if (buffer[length - 1] != '\n') {
            trigramSet.markBad();
            return;
        }

        addToSet(trigramSet, buffer, length);

        if (trigramSet.set.size() > MAX_TRIGRAMS) {
            trigramSet.markBad();
            return;
        }
    }
}

void TrigramSet::addToSet(TrigramSet& trigramSet, char* buffer, qint64 length)
{
    if (length < 3) {
        return;
    }

    for (qint64 i = 0; i < length - 2; ++i) {
        trigramSet.set.insert((static_cast<quint32>(buffer[i]) << 16) | (static_cast<quint32>(buffer[i + 1]) << 8) | static_cast<quint32>(buffer[i + 2]));
    }
}

qint64 TrigramSet::boyerMoore(const QString& text, const QString& source)
{
    qint64 ans = 0;
    std::string stdText = text.toStdString();
    std::string stdSource = source.toStdString();
    auto it = std::search(stdText.begin(), stdText.end(), std::boyer_moore_searcher(stdSource.begin(), stdSource.end()));
    while (it != stdText.end()) {
        ans++;
        it = std::search(it + 1, stdText.end(), std::boyer_moore_searcher(stdSource.begin(), stdSource.end()));
    }
    return ans;
}
