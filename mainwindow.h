#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "trigramset.h"

#include <QMainWindow>

#include <QFileInfo>
#include <memory>

#include <QFileSystemWatcher>
#include <QFutureWatcher>
#include <QTreeWidgetItem>

namespace Ui {
class MainWindow;
}

class WorkingData : public QObject {
    Q_OBJECT

public:
    QString dir;
    QSet<QString> supdirs;
    QSet<QString> files;

    QFileSystemWatcher watcher;

    QVector<TrigramSet> trigramsets;

    WorkingData(QString dir);

private slots:
    inline void setFileUpdateFlag(QString path) { files.insert(path); }
    inline void setSubdirUpdateFlag(QString path) { supdirs.insert(path); }
};

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    enum State {
        NoWork,
        WaitInputLine,
        WaitResult
    };

    explicit MainWindow(QWidget* parent = 0);
    ~MainWindow();
public slots:
    void addLine(QString line, qint64 count);

private slots:
    void showAboutQTDialog();
    void showInstructionDialog();

    void selectDirectory();
    void startSearch();
    void refresh();
    void clearAll();

    void endWork();
    void stopProcess();

    void openItemFile(QTreeWidgetItem* item);
    void showContextMenu(QPoint const& position);

private:
    std::unique_ptr<Ui::MainWindow> ui;
    State currentState;
    std::shared_ptr<WorkingData> data;
    QAtomicInt needStopProcess;

    QFutureWatcher<void> scaner;
    QFutureWatcher<void> indexer;
    QFutureWatcher<void> filter;

    QFutureWatcher<void> searcher;

    void startScaning();
    void doScan();
    void endScaning();
    void endIndexing();

    void doSearch(const TrigramSet& trigramSet, TrigramSet source, QString inputLine);

    void updateState(State state);
    void functionality(bool activityMode, bool processMode);

    void copyPath();
    QTreeWidgetItem* getSelectedItem();
    void findContextForOpen();

signals:
};

#endif // MAINWINDOW_H
